# FullStack Inadimplentes

Entre na pasta e execute o comando a seguir para iniciar o servidor:

```sh
# Comando para criar as imagens
docker-compose build

# Comando para subir o servidor
docker-compose up -d --build
```

Entre na URL [http://localhost:8080](http://localhost:8080)

Na primeira vez estará sem dados.
É só clicar no botão Gerar Dados para gerar nomes aleatórios.



### Alguns comandos úteis

```sh
# Comando para criar as imagens
docker-compose build

# Comando para subir o servidor
docker-compose up -d

# Comando para parar o servidor
docker-compose stop

# Comando para iniciar o servidor novamente
docker-compose start

# Comando para parar finalizar o servidor
docker-compose down

# Comando para finalizar o servidor e excluir todos os dados
docker-compose down --rmi local -v

# Comando para excluir todas as imagens temporárias sem TAG
docker image prune -f



# Comando para ver os logs da API
docker logs -f teste-fullstack-inadimplentes_api_1

# Comando para entrar no container da API
docker exec -it teste-fullstack-inadimplentes_api_1 sh
```
