const logger = require('../logger');
const Inadimplente = require('../models/Inadimplente');
const generateData = require('../util/generateData');

const fields = ['name', 'amount', 'date'];

module.exports = {
    async index(req, res) {
        const find = {};
        const sort = {};

        let { q, orderBy } = req.query;

        if (q) {
            find.name = new RegExp(q.replace(/([.+*?{}[\]()^$\\])/g, '\\$1'), 'i');
        }

        if (orderBy) {
            let [field, dir] = orderBy.split(':');

            if (fields.indexOf(field) === -1) {
                return res.status(400).json({ message: `Field '${field}' not exists.` });
            }

            sort[field] = dir === 'desc' ? -1 : 1;
        }

        const inadimplentes = await Inadimplente.find(find, null, { sort });

        return res.json(inadimplentes);
    },

    async seed(req, res) {
        let len = await Inadimplente.estimatedDocumentCount();

        logger.info({msg: 'Count Inadimplentes', len});

        if (len) {
            return res.json({ message: `Inadimplentes já inseridos na base de dados. (Quantidade atual: ${len})` });
        }

        const data = await generateData(100);
        const inserted = await Inadimplente.insertMany(data);
        len = inserted.length;

        logger.info({msg: 'Inadimplentes inseridos', len});

        res.json({ message: `${len} Inadimplentes inseridos com sucesso!`, ok: true });
    }
};
