const config = require('config');
const mongoose = require('mongoose');

const connect = async () => await mongoose.connect(config.get('App.database.mongoUrl'), {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const close = async () => await mongoose.connection.close();

module.exports = {
    connect,
    close,
};
