const config = require('config');
const App = require('./app');
const logger = require('./logger');
const graceful = require('./graceful');

graceful(async () => {
    const app = new App(config.get('App.port'));
    await app.init();
    app.start();

    return async () => {
        logger.debug('Closing server...');
        await app.close();
        logger.debug('Server closed.');
    };
});
