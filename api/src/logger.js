const config = require('config');
const pino = require('pino');

module.exports = pino(config.get('App.logger'));
