const mongoose = require('mongoose');

const InadimplenteSchema = new mongoose.Schema({
    name: String,
    amount: Number,
    date: Date,
});

module.exports = mongoose.model('Inadimplente', InadimplenteSchema);
