const { Router } = require('express');

const InadimplentesController = require('./controllers/InadimplentesController');

const routes = Router();

routes.get('/inadimplentes', InadimplentesController.index);
routes.get('/seed', InadimplentesController.seed);

module.exports = routes;
