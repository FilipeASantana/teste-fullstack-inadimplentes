const database = require('./database');
const logger = require('./logger');
const generateData = require('./util/generateData');
const Inadimplente = require('./models/Inadimplente');

async function seed() {
    let len = await Inadimplente.estimatedDocumentCount();

    logger.info({msg: 'Count Inadimplentes', len});

    if (len) {
        return;
    }

    const data = await generateData(100);
    const inserted = await Inadimplente.insertMany(data);
    len = inserted.length;

    logger.info({msg: 'Inadimplentes inseridos', len});
}

async function init() {
    try {
        await database.connect();
        await seed();
    } catch (e) {
        logger.error(e);
    }

    process.exit(0);
}

init();
