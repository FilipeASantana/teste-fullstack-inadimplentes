const axios = require('axios');
const rand = require('./rand');

async function generateData(qtd = 100) {
    const api = await axios.get(`https://www.wjr.eti.br/nameGenerator/index.php?q=${qtd}&o=json`);

    const list = api.data;

    list.sort(() => rand(-1, 1));

    return list.map(name => ({
        name,
        amount: rand(15, 257) + rand(0, 99) * .01,
        date: new Date(rand(2018, 2020), rand(0, 11), rand(1, 31)),
    }));
}

module.exports = generateData;
