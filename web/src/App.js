import { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import api from './services/api';
import Table from './components/Table';
import Search from './components/Search';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {
    const [loading, setLoading] = useState(true);
    const [inadimplentes, setInadimplentes] = useState([]);

    const [search, setSearch] = useState('');
    const [orderBy, setOrderBy] = useState('');
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const [alertOpen, setAlertOpen] = useState(false);
    const [alertSeverity, setAlertSeverity] = useState('');
    const [alertMessage, setAlertMessage] = useState('');

    async function loadData() {
        setLoading(true);

        const params = {
            q: search,
            orderBy,
        };

        const response = await api.get('/inadimplentes', { params });
        setInadimplentes(response.data);

        setLoading(false);
    }

    useEffect(() => {
        loadData();
    }, [search, orderBy]);

    const handleClick = async () => {
        setLoading(true);

        const response = await api.get('/seed');

        const { message, ok } = response.data;

        if (message) {
            setAlertSeverity(ok ? 'success' : 'info');
            setAlertMessage(message);
            setAlertOpen(true);
        }

        if (ok) {
            loadData();
        } else {
            setLoading(false);
        }
    };

    const handleAlertClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setAlertOpen(false);
    };

    const handleRequestSearch = value => {
        setPage(0);
        setSearch(value);
    };
    
    const handleRequestSort = (event, property) => {
        if (orderBy === property + ':desc') {
            setOrderBy('');
            setPage(0);
            return;
        }
        
        const desc = orderBy === property ? ':desc' : '';
        setOrderBy(property + desc);
        setPage(0);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <div className="App">
            <Button variant="contained" color="primary" onClick={handleClick}>Gerar Dados</Button>

            <Snackbar open={alertOpen} autoHideDuration={6000} onClose={handleAlertClose}>
                <Alert onClose={handleAlertClose} severity={alertSeverity}>{alertMessage}</Alert>
            </Snackbar>

            <Search loading={loading} onRequestSearch={handleRequestSearch} />

            <Table
                rows={inadimplentes}
                orderBy={orderBy}
                page={page}
                rowsPerPage={rowsPerPage}
                onRequestSort={handleRequestSort}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

export default App;
