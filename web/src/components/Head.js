import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from '@material-ui/core';

function Head({ headCells, classes, orderBy, onRequestSort }) {
    const [order, direction] = `${orderBy}:asc`.split(':');

    const cellMap = ({ id, label, align, disablePadding }) => (
        <TableCell
            key={id}
            align={align || 'left'}
            padding={disablePadding ? 'none' : 'default'}
            sortDirection={order === id ? direction : false}
        >
            <TableSortLabel
                active={order === id}
                direction={order === id ? direction : 'asc'}
                onClick={event => onRequestSort(event, id, direction)}
            >
                {label}
                {order === id ? (
                    <span className={classes.visuallyHidden}>
                        {direction === 'desc' ? 'sorted descending' : 'sorted ascending'}
                    </span>
                ) : null}
            </TableSortLabel>
        </TableCell>
    );

    return (
        <TableHead>
            <TableRow>{headCells.map(cellMap)}</TableRow>
        </TableHead>
    );
}

export default Head;
