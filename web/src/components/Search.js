import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    CircularProgress,
    IconButton,
    InputBase,
    Paper,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
}));

function Search({ loading, onRequestSearch }) {
    const [search, setSearch] = useState('');
    const classes = useStyles();

    const handleSubmit = event => {
        event.preventDefault();
        onRequestSearch(search);
    };

    const handleSearch = async event => {
        setSearch(event.target.value);
    };

    return (
        <Paper component="form" className={classes.root} onSubmit={handleSubmit}>
            <InputBase
                className={classes.input}
                placeholder="Buscar Clientes Inadimplentes"
                inputProps={{ 'aria-label': 'buscar clientes inadimplentes' }}
                value={search}
                onChange={handleSearch}
            />
            {
                loading ?
                    <CircularProgress size={44} />
                    :
                    <IconButton type="submit" className={classes.iconButton} aria-label="buscar">
                        <SearchIcon />
                    </IconButton>
            }
        </Paper>
    );
}

export default Search;
