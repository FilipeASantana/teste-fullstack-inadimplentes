import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    Table as UITable,
    TableBody,
    TableCell,
    TableContainer,
    TablePagination,
    TableRow,
} from '@material-ui/core';
import format from 'date-fns/format';
import Head from './Head';

const headCells = [
    { id: 'name', align: 'left', disablePadding: true, label: 'Nome do Cliente' },
    { id: 'amount', align: 'right', disablePadding: false, label: 'Valor' },
    { id: 'date', align: 'center', disablePadding: false, label: 'Inadimplente Desde' },
];

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function Table({ rows, orderBy, page, rowsPerPage, onRequestSort, onChangePage, onChangeRowsPerPage }) {
    const classes = useStyles();

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <TableContainer>
                    <UITable
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size="medium"
                        aria-label="enhanced table"
                    >
                        <Head
                            headCells={headCells}
                            classes={classes}
                            orderBy={orderBy}
                            onRequestSort={onRequestSort}
                        />
                        <TableBody>
                            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(({ _id, name, amount, date }) => {
                                    return (
                                        <TableRow hover tabIndex={-1} key={_id}>
                                            <TableCell component="th" scope="row" padding="none">
                                                {name}
                                            </TableCell>
                                            <TableCell align="right">
                                                R$ {amount.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+,)/, '$1.')}
                                            </TableCell>
                                            <TableCell align="center">{format(new Date(date), 'dd/MM/yyyy')}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 53 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </UITable>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 15, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={onChangePage}
                    onChangeRowsPerPage={onChangeRowsPerPage}
                />
            </Paper>
        </div>
    );
}
